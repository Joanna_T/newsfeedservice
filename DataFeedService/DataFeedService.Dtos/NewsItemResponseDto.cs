﻿using System;

namespace DataFeedService.Dtos
{
    
    public class NewsItemResponseDto
    {
        public String Title { get; set; }
        public String Description { get; set; }
        public Uri Link { get; set; }

        public String Date { get; set; }
        public String Time { get; set; }

        // What's a guid in this context?
        public String Guid { get; set; }

        public Uri WapLink { get; set; }
        
        // These are empty in all response items - what type are they
        public String VgImage { get; set; }
        public String ArticleImage { get; set; }
        public String Image { get; set; }

        // type please
        public String Body { get; set; }
    }
}
