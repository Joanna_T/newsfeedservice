﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DataFeedService.Mappers;
using DataFeedService.Services;
using NUnit.Framework;
using Rhino.Mocks;

namespace DataFeedService.Tests.Services
{
    [TestFixture]
    public class NewsDataServiceTests
    {
        private IDataFeedClient _client1;
        private IDataFeedClient _client2;
        private INewsItemMapper _mapper;

        private NewsDataService _service;

        [SetUp]
        public void Setup()
        {
            _client1 = MockRepository.GenerateMock<IDataFeedClient>();
            _client2 = MockRepository.GenerateMock<IDataFeedClient>();
            _mapper = MockRepository.GenerateMock<INewsItemMapper>();

            _service = new NewsDataService(new [] {_client1, _client2}, _mapper);
        }

        [Test]
        public void GetNews_Success_OneSourceFailed()
        {
            _client1
                .Stub(c => c.GetData())
                .Return(new ClientResponse
                {StatusCode = HttpStatusCode.InternalServerError});

            _client2
                .Stub(c => c.GetData())
                .Return(new ClientResponse { StatusCode = HttpStatusCode.OK, Content = new List<ClientRawResponseItemDto> {new ClientRawResponseItemDto(), new ClientRawResponseItemDto()}});

            Assert.DoesNotThrow(() => _service.GetNews());

            _client1.AssertWasCalled(c => c.GetData());
            _client2.AssertWasCalled(c => c.GetData());
            _mapper.AssertWasCalled(m => m.MapNewsItem(Arg<ClientRawResponseItemDto>.Is.Anything), opt => opt.Repeat.Twice());
        }

        // TODO: other tests
    }
}
