﻿namespace DataFeedService.Services
{
    public interface IDataFeedClient
    {
        ClientResponse GetData();
    }
}
