﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Castle.Core;
using DataFeedService.Dtos;
using DataFeedService.Exceptions;
using DataFeedService.Mappers;

namespace DataFeedService.Services
{
    public class NewsDataService : INewsDataService
    {
        private readonly IEnumerable<IDataFeedClient> _clients;
        private readonly INewsItemMapper _itemMapper;

        public NewsDataService(IEnumerable<IDataFeedClient> clients, INewsItemMapper itemMapper)
        {
            _clients = clients;
            _itemMapper = itemMapper;
        }

        // TODO: what's the expected behaviour in case when one of the data sources is not available? return data from other sources or fail the whole request?

        public List<NewsItemResponseDto> GetNews()
        {
            var clientCallTasks = new List<Task<ClientResponse>>();

            foreach (var client in _clients)
            {
                var clientSafe = client;
                var task = Task.Factory.StartNew(() => clientSafe.GetData());
                clientCallTasks.Add(task);
            }

            Task.WaitAll(clientCallTasks.ToArray());

            // TODO: here should follow response validation (response code, but also the actual content based on what is considered valid) and mapping to response objects

            if (clientCallTasks.All(t => t.Result.StatusCode != HttpStatusCode.OK))
                throw new UnableToRetrieveNewsDataException("All data sources failed to return a valid response");

            var newsObjects = new List<NewsItemResponseDto>();
            foreach (var task in clientCallTasks)
            {
                var clientResponse = task.Result;
                if (clientResponse.StatusCode != HttpStatusCode.OK)
                {
                    // TODO: log
                    continue;
                }

                clientResponse.Content.ForEach(it =>
                {
                    if (it == null)
                        // TODO: log? do we care?
                        return;

                    var mappedItem = _itemMapper.MapNewsItem(it);
                    newsObjects.Add(mappedItem);
                });
            }

            return newsObjects;
        }
    }
}