﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataFeedService.Dtos;

namespace DataFeedService.Services
{
    public class NewsFilter : INewsFilter
    {
        private readonly Func<NewsItemResponseDto, String, String, bool> _matchesFilteringCriteria = 
            (news, title, descr) => Matches(title, news.Title) 
                                    && Matches(descr, news.Description);

        public List<NewsItemResponseDto> FilterNews(List<NewsItemResponseDto> news, String title, String description)
        {
            return news
                .Where(n => _matchesFilteringCriteria(n, title, description))
                .ToList();
        }

        private static bool Matches(string filter, string input)
        {
            if (String.IsNullOrWhiteSpace(filter))
                return true;

            return !String.IsNullOrWhiteSpace(input) 
                        && input.ToLowerInvariant().Contains(filter.ToLowerInvariant());
        }
    }
}