using System;
using System.Collections.Generic;
using DataFeedService.Dtos;
using DataFeedService.Ioc;

namespace DataFeedService.Services
{
    public interface INewsFilter : IService
    {
        List<NewsItemResponseDto> FilterNews(List<NewsItemResponseDto> news, String title, String description);
    }
}