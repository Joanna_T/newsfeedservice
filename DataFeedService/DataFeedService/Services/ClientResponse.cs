﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using DataFeedService.Dtos;

namespace DataFeedService.Services
{
    // TODO: make this a pretty object with ctor and methods to set stuff rather than everything so exposed
    // TODO: make content generic if ever required
    public class ClientResponse
    {
        public HttpStatusCode StatusCode { get; set; }

        public IEnumerable<ClientRawResponseItemDto> Content { get; set; }

        public Exception Exception { get; set; }

        public String Error { get; set; }

        public String GetError()
        {
            if (Error != null)
                return Error;

            if (Exception != null)
                return String.Format("{0}: {1}", (object) Exception.GetType(), (object) Exception.Message);

            return null;
        }
    }
}