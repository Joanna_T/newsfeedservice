﻿using System;
using System.Collections.Generic;
using System.Net;

namespace DataFeedService.Services
{
    public class VgFeedClient : IDataFeedClient
    {
        // todo: move to an injected config file
        private String _feedUrl = "http://www.vg.no/rss/feed/forsiden/?frontId=1";

        // TODOs - things to confirm:
        /*
         * 1. data types of the items in the feed
         * 2. validation against some vg schema? what info needs to be present for the item to be considered valid?
         * 3. if any of the items are invalid, should they just be filtered out or the whole response discarded?
         * 4. what to do in case when description or title are null? should filtering consider it a non match?
         */

        // TODO: implement the actual service call
        public ClientResponse GetData()
        {
            var content = new List<ClientRawResponseItemDto>
            {
                new ClientRawResponseItemDto() { Title = "Foo", Description = "Bar"}
            };

            return new ClientResponse
            {
                StatusCode = HttpStatusCode.OK,
                Content = content
            };
        }
    }
}