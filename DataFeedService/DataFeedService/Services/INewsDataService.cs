﻿using System;
using System.Collections.Generic;
using DataFeedService.Dtos;
using DataFeedService.Ioc;

namespace DataFeedService.Services
{
    public interface INewsDataService : IService
    {
        List<NewsItemResponseDto> GetNews();
    }
}