﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using DataFeedService.Dtos;

namespace DataFeedService.Services
{
    public class TestFeedClient : IDataFeedClient
    {
        // TODO: move to an injected config file
        private string _url = "http://rexxars.com/playground/testfeed/";

        // TODO: what validation is required on the items, when to filter anything out?
        // TODO: how can the API fail? 

        // TODO: create a client wrapper to enable injection, move exception handling into a base class for all clients
        public ClientResponse GetData()
        {
            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri(_url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var clientResponse = new ClientResponse();

                try
                {
                    // NOTE: forcing synchronous execution as we want to get the result
                    // we could return a Task instead and perform the rest of the actions in nested ContinueWith-s 
                    var response = client.GetAsync(_url).Result;
                    clientResponse.StatusCode = response.StatusCode;

                    if (response.Content is StreamContent)
                    {
                        try
                        {
                            var news = response.Content.ReadAsAsync<IEnumerable<ClientRawResponseItemDto>>().Result;
                            clientResponse.Content = news;
                        }
                        catch (Exception ex)
                        {
                            clientResponse.Exception = ex;
                        }

                        return clientResponse;
                    }

                    if (response.Content is StringContent)
                    {
                        var error = response.Content.ReadAsStringAsync().Result;
                        clientResponse.Error = error;
                        return clientResponse;
                    }

                    return clientResponse;
                }
                catch (AggregateException ex)
                {
                    Exception exception = null;

                    if (ex.InnerExceptions != null && ex.InnerExceptions.Any())
                    {
                        exception = ex.InnerExceptions.First();
                        if (exception.InnerException != null)
                            exception = exception.InnerException;
                    }

                    clientResponse.StatusCode = HttpStatusCode.InternalServerError;
                    clientResponse.Error = exception != null
                                            ? exception.ToString()
                                            : "Encountered an AggreggateException without any inner exceptions";
                    return clientResponse;
                }
                
            }
        }
    }
}