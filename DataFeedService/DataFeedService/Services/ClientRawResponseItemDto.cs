﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataFeedService.Services
{
    // NOTE: separated out from the (very similar) service response dto in case the format returned from the client changes in future
    public class ClientRawResponseItemDto
    {
        public String Title { get; set; }
        public String Description { get; set; }
        public Uri Link { get; set; }

        public String Date { get; set; }
        public String Time { get; set; }

        // What's a guid in this context?
        public String Guid { get; set; }

        public Uri WapLink { get; set; }

        // These are empty in all response items - what type are they
        public String VgImage { get; set; }
        public String ArticleImage { get; set; }
        public String Image { get; set; }

        // type please
        public String Body { get; set; }

        // TODO: split into date and time
        public DateTime PubDate { get; set; }
    }
}