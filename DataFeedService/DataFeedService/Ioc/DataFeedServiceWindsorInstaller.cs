﻿using System.Reflection;
using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using DataFeedService.Controllers;
using DataFeedService.Services;

namespace DataFeedService.Ioc
{
    public class DataFeedServiceWindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                // Install controllers 
                AllTypes
                    .FromAssembly(Assembly.GetExecutingAssembly())
                    .BasedOn<IWebApiController>()
                    .Configure(c => c.LifeStyle.Is(LifestyleType.PerWebRequest))
                    .WithService.Self(),

                AllTypes
					.FromAssembly(Assembly.GetExecutingAssembly()).IncludeNonPublicTypes()
					.BasedOn<IService>()
					.Configure(c => c.LifeStyle.Is(LifestyleType.PerWebRequest))
					.WithService.FirstInterface(),

                AllTypes
                    .FromAssembly(Assembly.GetExecutingAssembly()).IncludeNonPublicTypes()
                    .BasedOn<ISingletonService>()
                    .Configure(c => c.LifeStyle.Is(LifestyleType.Singleton))
                    .WithService.FirstInterface(),

                AllTypes
					.FromAssembly(Assembly.GetExecutingAssembly()).IncludeNonPublicTypes()
					.BasedOn<IDataFeedClient>()
					.Configure(c => c.LifeStyle.Is(LifestyleType.PerWebRequest))
					.WithService.FirstInterface()
                );

            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel));
        }
    }
}