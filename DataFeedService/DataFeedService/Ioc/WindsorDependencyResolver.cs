﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;
using Castle.Windsor;

namespace DataFeedService.Ioc
{
    public class WindsorDependencyResolver : IDependencyResolver
    {
        private readonly IWindsorContainer _container;

		public WindsorDependencyResolver(IWindsorContainer container)
		{
			_container = container;
		}

		/// <summary>
		/// Retrieves a service from the scope.
		/// </summary>
		/// <returns>
		/// The retrieved service.
		/// </returns>
		/// <param name="serviceType">The service to be retrieved.</param>
		/// <remarks>
		/// Returns null if unresolved
		/// </remarks>
		public object GetService(Type serviceType)
		{
			if (!_container.Kernel.HasComponent(serviceType))
				return null;

			return _container.Resolve(serviceType);
		}

		/// <summary>
		/// Retrieves a collection of services from the scope.
		/// </summary>
		/// <returns>
		/// The retrieved collection of services.
		/// </returns>
		/// <param name="serviceType">The collection of services to be retrieved.</param>
		/// <remarks>
		/// Returns empty enumerable if unresolved
		/// </remarks>
		public IEnumerable<object> GetServices(Type serviceType)
		{
			if (!_container.Kernel.HasComponent(serviceType))
				return new object[0];

			return (IEnumerable<object>)_container.ResolveAll(serviceType);
		}

		public IDependencyScope BeginScope()
		{
			return this;
		}

		public void Dispose()
		{
		}
    }
}