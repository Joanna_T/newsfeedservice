﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using DataFeedService.Dtos;
using DataFeedService.Services;
using Newtonsoft.Json;

namespace DataFeedService.Controllers
{
    public class NewsController : ApiController, IWebApiController
    {
        private readonly INewsDataService _newsDataService;
        private readonly INewsFilter _newsFilter;

        public NewsController(INewsDataService newsDataService, INewsFilter newsFilter)
        {
            _newsDataService = newsDataService;
            _newsFilter = newsFilter;
        }

        // TODO: what's the min num chars to kick off filtering? - current assumption (for latin alphabets) is at least 2 chars, otherwise discard request
        public HttpResponseMessage GetNews(String title = null, String description = null)
        {
            String titleFilter = null;
            String descriptionFilter = null;

            if (!String.IsNullOrWhiteSpace(title))
            {
                titleFilter = HttpUtility.UrlDecode(title);
                if (!HasAllowedLength(titleFilter))
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Title must be at least 2 characters long");
            }

            if (!String.IsNullOrWhiteSpace(description))
            {
                descriptionFilter = HttpUtility.UrlDecode(description);
                if (!HasAllowedLength(descriptionFilter))
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Description must be at least 2 characters long");
            }

            var allNews = _newsDataService.GetNews();

            List<NewsItemResponseDto> filteredNews = null;
            if (title != null || description != null)
                // TODO: consider if we can filter at any earlier point to not retrieve all the data or at least cut down processing of it as soon as possible
                filteredNews = _newsFilter.FilterNews(allNews, titleFilter, descriptionFilter);

            return Request.CreateResponse(HttpStatusCode.OK, filteredNews ?? allNews, new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore}
            });
        }

        // TODO: confirm the actual rules rather than using some made up ones
        private bool HasAllowedLength(String filterCandidate)
        {
            return filterCandidate.Length > 1 && filterCandidate.Length < 255;
        }
    }
}