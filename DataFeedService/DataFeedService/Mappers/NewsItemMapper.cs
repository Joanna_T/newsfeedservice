﻿using DataFeedService.Dtos;
using DataFeedService.Services;

namespace DataFeedService.Mappers
{
    public class NewsItemMapper : INewsItemMapper
    {
        // TODO: This will eventually have the logic to map PubDate from the other service into Date and Time
        public NewsItemResponseDto MapNewsItem(ClientRawResponseItemDto rawItem)
        {
            return new NewsItemResponseDto
            {
                Title = rawItem.Title,
                Description = rawItem.Description

                // TODO: rest of the fields here
            };
        }
    }
}