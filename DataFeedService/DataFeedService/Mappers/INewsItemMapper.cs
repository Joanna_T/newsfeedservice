﻿using DataFeedService.Dtos;
using DataFeedService.Ioc;
using DataFeedService.Services;

namespace DataFeedService.Mappers
{
    public interface INewsItemMapper : ISingletonService
    {
        NewsItemResponseDto MapNewsItem(ClientRawResponseItemDto rawItem);
    }
}