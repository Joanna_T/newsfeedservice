﻿using System;

namespace DataFeedService.Exceptions
{
    public class UnableToRetrieveNewsDataException : Exception
    {
        public UnableToRetrieveNewsDataException(String message) : base(message)
        {
        }
    }
}